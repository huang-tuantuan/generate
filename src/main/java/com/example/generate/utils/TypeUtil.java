package com.example.generate.utils;


import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 类型转换工具
 * @author HTT
 */
@Component
public class TypeUtil {

    /**
     * 数据库字段属性转换为Java字段属性（MySQL）
     * 例如varchar -> String
     * @param dataType
     */
    public static String dataTypeToJavaType(String dataType)
    {
        Map<String, String> typeMapping = new HashMap<>(16);
        typeMapping.put("varchar", "String");
        typeMapping.put("int", "Integer");
        typeMapping.put("tinyint", "Integer");
        typeMapping.put("smallint", "Integer");
        typeMapping.put("mediumint", "Integer");
        typeMapping.put("bigint", "Long");
        typeMapping.put("bit", "Boolean");
        typeMapping.put("float", "BigDecimal");
        typeMapping.put("double", "BigDecimal");
        typeMapping.put("decimal", "BigDecimal");
        typeMapping.put("char", "String");
        typeMapping.put("text", "String");
        typeMapping.put("blob", "byte[]");
        typeMapping.put("date", "Date");
        typeMapping.put("datetime", "Date");
        typeMapping.put("timestamp", "Timestamp");
        String str = typeMapping.get(dataType);
        if(StringUtils.isEmpty(str)){
            return "String";
        }
        return str;
    }
}
