package com.example.generate.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * 文字工具类
 * @author HTT
 */
@Component
public class TextUtil {

    public static String capitalizeFirstLetter(String original) {
        if (StringUtils.isEmpty(original)) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }
}
