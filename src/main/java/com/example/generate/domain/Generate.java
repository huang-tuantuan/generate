package com.example.generate.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 生成代码需要的一些属性
 * @author HTT
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Generate {

    /**
     * 表主键
     */
    private String primaryKey;

    /**
     * 表主键转Java驼峰
     */
    private String javaKey;

    /**
     * 主键类型
     */
    private String keyType;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表名转Java驼峰
     */
    private String javaTableName;

    /**
     * 实体类名
     */
    private String className;

    /**
     * 表信息
     */
    private List<TableColumn> tableColumnList;
}
