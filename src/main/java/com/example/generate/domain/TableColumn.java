package com.example.generate.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 指定表信息的三个核心属性
 * @author HTT
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TableColumn {

    /**
     * 数据库的列名
     */
    private String columnName;

    /**
     * 数据库的列名转Java驼峰命名
     */
    private String javaColumnName;

    /**
     * 数据库的列类型
     */
    private String dataType;

    /**
     * 数据库的列类型Java类型
     */
    private String javaType;

    /**
     * 列注释
     */
    private String columnComment;

}
