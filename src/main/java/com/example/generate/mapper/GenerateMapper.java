package com.example.generate.mapper;

import com.example.generate.domain.TableColumn;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 生成代码的数据库交互层
 * @author HTT
 */
@Mapper
public interface GenerateMapper {

    /**
     * 根据提供的表名称去查询表的字段、类型和注释信息
     * @param tableName
     * @return
     */
    public List<TableColumn> selectByTableName(String tableName);
}
