package com.example.generate;

import com.example.generate.service.GenerateService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class TableColumnApplicationTests {

    @Resource
    private GenerateService generateService;

    @Value("${generate.tableName}")
    private String tableName;

    @Test
    void contextLoads() throws Exception {
        generateService.generate(tableName);
    }

}
